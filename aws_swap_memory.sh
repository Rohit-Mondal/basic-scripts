clear
echo "Starting Process Of Creating Swap Memory" 
echo "Step 1 : Creating Swap File....."
sudo dd if=/dev/zero of=/swapfile bs=128M count=32
echo "Step 2: Updating Read & Write Permissions....."
sudo chmod 600 /swapfile
echo "Step 3: Setting up Linux swap area....."
sudo mkswap /swapfile
echo "Step 4: Making Swap File available for immediate use....."
sudo swapon /swapfile
echo "Step 5: Verifying......"
sudo swapon -s
echo "Step 6: Finalizing....."
sudo su -c "echo '/swapfile swap swap defaults 0 0' >> /etc/fstab"
echo "Success..Swap Memory Created Sucessfully"