#!/bin/bash

export CONTAINER_NAME="MongoDB"
export BACKUP_LOCATION="/home/jnctc/db_backups"
export DOCKER_LOCATION="/data/backups"
export DATABASE_NAME="jnctc"

export TIMESTAMP=$(date +'%Y%m%d')

echo Starting Auto Backup Process ...

# docker exec -t ${CONTAINER_NAME} mongodump --authenticationDatabase admin --username root --password example --out /data/backup-${TIMESTAMP}
docker exec -t ${CONTAINER_NAME} mongodump --out ${DOCKER_LOCATION}/backup-${TIMESTAMP}
docker cp ${CONTAINER_NAME}:${DOCKER_LOCATION}/backup-${TIMESTAMP} ${BACKUP_LOCATION}

cd ${BACKUP_LOCATION}
echo Zipping...
zip -r backup-${TIMESTAMP}.zip backup-${TIMESTAMP}
echo Deleting Folder backup-${TIMESTAMP}
find backup-${TIMESTAMP} -delete
docker exec MongoDB rm -rf ${DOCKER_LOCATION}/backup-${TIMESTAMP}
echo Done...
